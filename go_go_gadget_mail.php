<?php
header('Access-Control-Allow-Origin: *');

require __DIR__ . '/vendor/autoload.php';

try{
    $incomeParams = filterparams($_POST);
}catch (Exception $e){
    die(json_encode(['error'=>'1','message'=>$e->getMessage()]));
}

$mailGun = new \Mailgun\Mailgun($incomeParams['key']['mailGunKey']);
if($incomeParams['key']['to']){
    $buildedTemplate = buildTemplate($incomeParams,$incomeParams['key']['toTemplate']);
    $mailGun->sendMessage($incomeParams['key']['mailGunDomain'],
        array(
            'from'    => $incomeParams['key']['from'],
            'to'      => $incomeParams['key']['toBox'],
            'subject' => $incomeParams['key']['toTitle'],
            'html'    => $buildedTemplate
        )
    );
}
if($incomeParams['key']['reverse']&&!empty($incomeParams['email'])){
    $buildedTemplate = buildTemplate($incomeParams,$incomeParams['key']['reverseTemplate']);
    $mailGun->sendMessage($incomeParams['key']['mailGunDomain'],
        array(
            'from'    => $incomeParams['key']['from'],
            'to'      => $incomeParams['email'],
            'subject' => $incomeParams['key']['reverseTitle'],
            'html'    => $buildedTemplate
        )
    );
}
die(json_encode(['result'=>'ok']));


function buildTemplate($data,$templatePath){
    include $templatePath;
    $premailer = new ScottRobertson\Premailer\Request();
    $response = $premailer->convert($template);
    return $response->downloadHtml();
}

function filterparams($dataArray){
    require_once 'allowed_keys.php';
    $filteredValue = [];
    if(empty($dataArray['key'])||empty($allowedKeys[$dataArray['key']]))
        throw new Exception('That\'s a bad move, partner.');
    $keyParams =$allowedKeys[$dataArray['key']];
    unset($dataArray['key']);
    foreach ($dataArray as $key=>$value)
        $filteredValue[$key] = htmlspecialchars($value);
    $filteredValue['key'] = $keyParams;
    $filteredValue['now'] = date($keyParams['timeFormat']);
    return $filteredValue;
}